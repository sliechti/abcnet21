import numpy as np
import tensorflow as tf
import argparse
import os
import h5py as h5

strategy = tf.distribute.MirroredStrategy() #so that you can use multiple GPUs

parser = argparse.ArgumentParser()

parser.add_argument('--load_dir', type=str, default='/set/your/default', help = 'directory where to load the model from')
parser.add_argument('--h5_dir', type=str, default='/set/your/default', help = 'directory where to load h5 files from')
parser.add_argument('--model_name', type=str)
parser.add_argument('--eval_name', type=str, default='set_your_default_name', help='Name of the evaluation data file')
parser.add_argument('--batch_size', type=int, default=64, help='Batch size')
parser.add_argument('--save_dir', type=str, default='/set/your/default', help = 'directory where to save the output')
parser.add_argument('--output_name', type=str)
parser.add_argument('--use_global', default=False, action='store_true', help='Use global variables, i.e., per-event variables')

FLAGS = parser.parse_args()
load_dir = FLAGS.load_dir
h5_dir = FLAGS.h5_dir
model_name = FLAGS.model_name
eval_name = FLAGS.eval_name
batch_size = FLAGS.batch_size
save_dir = FLAGS.save_dir
output_name = FLAGS.output_name
use_global_variables = FLAGS.use_global 
model_path = os.path.join(load_dir, model_name)

with strategy.scope():

    #RESTORE THE MODEL
    model = tf.keras.models.load_model(model_path)
    s = 'MODEL ' + model_name + ' LOADED'
    ls = len(s)
    print('#'*(ls+4))
    print('# ' + s + ' #')
    print('#'*(ls+4))
    
    #LOAD THE PREDICTION FILES
    sets = ['pred']
    for set_ in sets:
        if set_ == 'pred':
            set_name = eval_name
            f = h5.File(os.path.join(h5_dir, set_name), 'r')
            x_MD_pred = np.array(f['data']) #suppose your multi-dimensional (MD), i.e., per-paraticle input is stored in a dataset called 'data'
            if use_global_variables:
                x_global_pred = np.array(f['global']) #if you use global variables, load coresponding dataset. Suppose per-event data is stored in a dataset called 'global'
        f.close()
    
    #MAKE PREDICTIONS
    if use_global_variables:
        y_score = model.predict((x_MD_pred, x_global_pred), batch_size=batch_size, verbose=1)
    else:
        y_score = model.predict((x_MD_pred), batch_size=batch_size, verbose=1)
    
save_path = dset = os.path.join(save_dir,'{0}.h5'.format(FLAGS.output_name))

with h5.File(os.path.join(save_dir,'{0}.h5'.format(FLAGS.output_name)), "w") as fh5 :
    dset = fh5.create_dataset("DNN", data=y_score) #store your predictions
    dset = fh5.create_dataset("data", data=x_MD_pred) #store input multdim data
    if use_global_variables:
        dset = fh5.create_dataset("data_global", data=x_global_pred) #store input global data
    
