########################################################################
# Authors: Sascha Liechti - University of Zurich
#          Fabio Iemmi    - IHEP Beijing
########################################################################

print("Importing libraries...")

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.python.keras.utils.layer_utils import count_params
import numpy as np
import argparse
import os
import h5py as h5
from abcnet.utils.ABCNet_utils import pairwise_distanceR, pairwise_distance, GAPBlock

print("Imports finished, script is starting")
 
strategy = tf.distribute.MirroredStrategy() #so that you can use multiple GPUs

#################### Read Flags ####################
parser = argparse.ArgumentParser()

parser.add_argument('--save_dir', type=str, default='/set/your/default',
                    help='Directory to save the model in')
parser.add_argument('--data_path', type=str, default='/set/your/default',
                    help='Directory where all the .h5 data is')
parser.add_argument('--train_name', type=str, default='default_name',
                    help='Name of the training data file')
parser.add_argument('--val_name', type=str, default='default_name',
                    help='Name of the validation data file')
parser.add_argument('--plots', default=False, action='store_true',
                    help='Create plots (import mathplotlib)')
parser.add_argument('--show', default=False, action='store_true',
                    help='show plots')
parser.add_argument('--use_global', default=False, action='store_true',
                    help='Use global variables, i.e., per-event variables')
parser.add_argument('--name', type=str, default='default_name',
                    help='Model name')
parser.add_argument('--batch_size', type=int, default=16,
                    help='Batch size')
parser.add_argument('--momentum', type=int, default=0.99,
                    help="Momentum for batch normalization layers")
parser.add_argument('--lr', type=float, default=0.001,
                    help='Initial learning rate')
parser.add_argument('--patience', type=int, default=10,
                    help='Patience parameter for the early stopping callback')

FLAGS = parser.parse_args()

save_dir = FLAGS.save_dir
data_path = FLAGS.data_path
train_name = FLAGS.train_name
val_name = FLAGS.val_name
plots = FLAGS.plots
show = FLAGS.show
model_name = FLAGS.name
batch_size = FLAGS.batch_size
momentum = FLAGS.momentum
lr = FLAGS.lr
patience = FLAGS.patience
use_global_variables = FLAGS.use_global

if plots:
    # Only needed if you want to plot things, you can also do without
    import matplotlib.pyplot as plt

#################### Load data ####################
s = 'TRAINING MODEL ' + model_name
ls = len(s)
print('#'*(ls+4))
print('# ' + s + ' #')
print('#'*(ls+4))

sets = ['train', 'val']

# Load scalers for targets if needed (it is often advantageous to scale from (-0.8, 0.8) for regression
# Centering the data often helps too

for set_ in sets:
    #load training set
    #############################
    ######## IMPORTANT ##########
    #############################
    #For the nearest neighbors gathering to happen properly, the first three features of your training set *MUST* be the following:
    #feature[0] = particle eta
    #feature[1] = particle phi
    #feature[2] = particle pt
    ############################
    ############################
    ############################
    if set_ == 'train':
        set_name = train_name
        f = h5.File(os.path.join(data_path, set_name), 'r')
        x_MD_train = np.array(f['data']) #suppose your multi-dimensional (MD), i.e., per-paraticle input is stored in a dataset called 'data'
        if use_global_variables:
            x_global_train = np.array(f['global']) #if you use global variables, load coresponding dataset. Suppose per-event data is stored in a dataset called 'global'
        #load labels
        y_train = np.array(f['label']) #suppose your labes are stored in a dataset called 'label'
        f.close()

    elif set_ == 'val':
        #repeat the same process for validation set
        set_name = val_name
        f = h5.File(os.path.join(data_path, set_name), 'r')
        x_MD_val = np.array(f['data'])
        if use_global_variables:
            x_global_val = np.array(f['global'])
        y_val = np.array(f['label'])
        f.close()

################## Callbacks ################## 
early_stopping_callback = keras.callbacks.EarlyStopping(patience = patience, restore_best_weights = True) #stop training if you don't see any improvement in validation loss after 'patience' number of epochs. Restore weights corresponding to be best epoch, i.e.,  the epoch showing the least degree of overfitting

################## Define the model  ##################################
with strategy.scope():
    # Define the shapes of the multidimensionanl inputs for the pointcloud (per particle variables)
    # Always leave out the batchsize when specifying the shape
    MDInput = keras.Input(shape=(np.shape(x_MD_train)[1], np.shape(x_MD_train)[2]))
    
    k = 20 # Number of nearest neighbours to aggregate in the GAP layers
        
    pt = MDInput[:,:,2] #take pt
    mask = tf.cast(tf.equal(pt, 0), dtype='float32') #define a mask for 0-padded particles, in such a way that they don't enter the k nearest neighbors gathering
    
    #compute adjoint matrix used by ABCNet GapLayers
    adj_1, zero_matrix = pairwise_distanceR(MDInput[:,:,:3], mask)
    
    #ABCNet GAPBlock
    #a GAPBlock needs three inputs:
    #1: adjoint matrix, used to gather nearest neighbors
    #2: 1st input of the layer itself, ABCNet operations will be performed on it
    #3: 2nd input of the layer itself, it will get concatenated with ABCNet neighbors features
    neighbors_features_1, graph_features_1, attention_features_1 = GAPBlock(k=k, filters_C2DNB=16, padding_C2DNB = 'valid', name='Gap1')((adj_1, MDInput, MDInput))
    
    aggregate_list = [] # Create list to concat later if you want to fuse multiple outputs
    aggregate_list.append(graph_features_1) # Like this we use only the graph features for the aggregation
    
    #Create 1D convolutional layer using the Keras FunctionalAPI syntax
    x = layers.Conv1D(filters = 64, kernel_size = 1, strides = 1, padding='valid', kernel_initializer='glorot_uniform', activation='relu')(neighbors_features_1)
    #Apply batch normaliztion
    x = layers.BatchNormalization(momentum=momentum)(x)

    aggregate_list.append(x)
    
    adj_2 = pairwise_distance(x, zero_matrix)
    
    #ABCNet GAPBlock
    neighbors_features_2, graph_features_2, attention_features_2 = GAPBlock(k=k, momentum=momentum, filters_C2DNB=64, padding_C2DNB = 'valid', name='Gap2')((adj_2, x, MDInput))

    aggregate_list.append(graph_features_2)

    x = layers.Conv1D(filters = 64, kernel_size = 1, strides = 1, padding='valid', kernel_initializer='glorot_uniform', activation='relu')(neighbors_features_2)
        
    x = layers.BatchNormalization(momentum=momentum)(x)

    aggregate_list.append(x)

    ################## Global Model Part ##################################
    if use_global_variables:

        # Specify the shape of the per event variables
        GlobalInput = keras.Input(shape=(np.shape(x_global_train)[1]))

        y = layers.Dense(20, activation='relu')(GlobalInput)
        y = layers.Dense(20, activation='relu')(y)
        GlobalOutput = layers.Dense(20, activation='linear')(y)
        _list = []
        for i in range(np.shape(x_MD_train)[1]):
            # Stack the output to make them compatible with tf.concat with the other outputs of the MD part
            _list.append(GlobalOutput)
        aggregate_list.append(tf.stack(_list, axis=-2)) # Add them to the aggregation
    
    #perform aggregation. Aggregation is a concat tf.operation
    x = tf.concat(aggregate_list, axis = -1) 

    x = layers.Conv1D(filters = 256, kernel_size = 1, strides = 1, padding='valid', kernel_initializer='glorot_uniform', activation='relu')(x)
        
    x = layers.BatchNormalization(momentum=momentum)(x)

    x_prime = x
    
    #Perform AveragePooling
    x = layers.AveragePooling1D(pool_size=50, strides=50, padding='valid')(x)
        
    expand=tf.tile(x, [1, 50, 1]) #after pooling, recover x tensor's second dimension by tiling

    x = tf.concat(values = [expand, x_prime], axis=-1)

    x = layers.Conv1D(filters = 256, kernel_size = 1, strides = 1, padding='valid', kernel_initializer='glorot_uniform', activation='relu')(x)
    x = layers.BatchNormalization(momentum=momentum)(x)

    FinalOutput = layers.Conv1D(filters = 1, kernel_size = 1, strides = 1,  padding='valid', kernel_initializer='glorot_uniform', activation='sigmoid')(x)

    ############## Create the model ##############
    if use_global_variables:
        PartJetModel = keras.Model(inputs=[MDInput, GlobalInput], outputs=FinalOutput, name=model_name)
    else:
        PartJetModel = keras.Model(inputs=[MDInput], outputs=FinalOutput, name=model_name)
    
    ############# Compile the model #############
    opt = tf.keras.optimizers.Adam(learning_rate=lr) #choose an optimizer...
    PartJetModel.compile(optimizer=opt, loss=tf.keras.losses.BinaryCrossentropy())#...and a loss
    
    #Print weight multiplicites for your model
    trainable_count = count_params(PartJetModel.trainable_weights)
    non_trainable_count = count_params(PartJetModel.non_trainable_weights)
    print('Total params: {:,}'.format(trainable_count + non_trainable_count))
    print('Trainable params: {:,}'.format(trainable_count))
    print('Non-trainable params: {:,}'.format(non_trainable_count))
    
################## Training ##################################
if True: # SET TO FALSE TO QUICKLY RUN THE CODE WITHOUT TRAINING AND DEBUG
    if use_global_variables:
        PartJetModel.fit((x_MD_train, x_global_train), y_train, validation_data=([x_MD_val, x_global_val], y_val), epochs=1, verbose=2, batch_size = batch_size)
    else:
        PartJetModel.fit((x_MD_train), y_train, validation_data=([x_MD_val], y_val), epochs=1, verbose=2, batch_size = batch_size)
    ############ Save results of the training ###########
    PartJetModel.save(os.path.join(save_dir, model_name))






