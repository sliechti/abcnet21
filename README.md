# A Tensorflow 2.x, Functional API compatible implementation of ABCNet

## Goal

The goal of this library is to improve usability and flexibilty of ABCNet [[1](https://arxiv.org/abs/2211.02029), [2](https://link.springer.com/article/10.1140%2Fepjp%2Fs13360-020-00497-3)]. Custom ABCNet layers (GAPLayers) have been made compatible with the [Keras Functional API](https://keras.io/guides/functional_api/), in such a way that users can ideally write any custom model including ABCNet-specific layers.
Toy examples are given on how to train and evaluate an ABCNet-like model using the Keras Functional API.

## Dependancies

*  Python 3.7 or higher
*  TensorFlow 2.1 or higher
*  h5py
*  NumPy

## Installation
We strongly recommend to install the library inside a virtual environment. A conda environment is probably the easiest way to go. If you don't have it already, [install Anaconda](https://docs.anaconda.com/anaconda/install/index.html) first. Then, create a virtual environment and activate it:
```
conda create --name abcnetenv
conda activate abcnetenv
```
Install the software needed to train on GPUs, i.e., [TensorFlow](https://www.tensorflow.org/) and [CUDA](https://developer.nvidia.com/cuda-toolkit):
```
conda install tensorflow-gpu>=2.1.0
```
Finally, pip-install the library: 

```
pip install git+https://gitlab.cern.ch/sliechti/abcnet21
```

This will install the abcnet library as well as all dependancies.

## General architecture of a model 

<img src="./abcnet/fig/Model-Scheme.png" alt="alt text" width="500"/>

An example of an ABCNet-like model is given in the picture above. It can be roughly divided into three sections: aggregation layers (dealing with per-particle features), global layers (dealing with per-event features) and post-aggregation layers (dealing with the aggregation of the prevously mentioned features).

#### Aggregation Layers
This section of the model contains the ABCNet custom layers, known as Graph Attention Pooling layers (GAPLayers). These layers perform the typical ABCNet actions (gathering of k-nearest neighbors, computation of attention features, ...). They can be accompanied by CNN layers such as `Conv1D`, `Conv2D`, `MaxPooling` and so on, or ordinary DNN layers such as `Dense` layers. 

#### Global Layers
This section of the model contains the layers dealing with the per-event (global) features.

#### PostAggregation Layers
This section of the model contains the layers coming after the results of aggregation and global layers have been merged together. 

#### Additional parameters for the architecture
* *k*: int, number specifying the particles (k nearest neighbours) gathered in the GAPLayers.

## Example code
### Training
A template code training a toy ABCNet model is given in `models/train_toyModel.py`. The code has been extensively commented to point out the main steps of the procedure. What's happening inside the code can roughly be summarized as follows:
* Load some libraries and parse arguments from command line. Please take some time to see which parameters can be set there;
* Load input training and validation data. If the user chose to use global variables, corresponding training and validation data are loaded too;
* Define the model. This section shows the power and flexibilty of the Keras Functional API. Layers (including GAPLayers) can be treated as functions, taking tensors as inputs and returning tensors as outputs, e.g.,
```
y = keras.layers.Dense(64, activation='relu')(x)
z = keras.layers.Dense(128, activation='relu')(y)
```
giving you great freedom in building complex custom models. In this section, depending on the tackled problem, the shape of the output is defined by the user;
* Create the model by giving inputs, outputs and a name for the model itself. Concerning the inputs, please be aware of the **following important notes**:
    * The per-particle input tensor should have shape `(n_events, n_particles, n_features)` and, in order for the neighbors gathering to work as expected, the first three features should be, _in this very same order_, particle η, φ, p<sub>T</sub> ;
    * The per-event variables get added to the input data as additional features. It is important that the per-event variables come after the per-partice features;
* Compile the model giving an optimizer and a loss function;
* Peform the training through the Keras `model.fit()` method;
* Save the trained model to a user-defined directory.

### Evaluation
A template code evaluating the previously mentioned toy model is given in `models/evaluate_toyModel.py`. The code has been extensively commented to point out the main steps of the procedure. What's happening inside the code can roughly be summarized as follows:
* Load some libraries and parse arguments from command line. Again, please take some time to see which parameters can be set there
* Load the trained model and the input evaluation data
* Make prediction through the Keras `model.predict()` method
* Save predictions to an output `.h5` file

## Using ABCNet? Let us know!
Feedback from users about any aspect of this repository is more than welcome. If you have comments, questions or suggestions feel free to get in touch with Fabio Iemmi (fabio.iemmi@cern.ch) and/or Sascha Liechti (sascha.liechti@cern.ch).
